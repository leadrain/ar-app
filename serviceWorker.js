const staticArApp = "ar-app-site-v1";
const assets = [
  "/ar-app",
  "/index.html",
  "/js/app.js",
  "/asset/favicon.png",
  "/asset/favicon1.png",
  "/asset/Astronaut.bin",
  "/asset/Astronaut.gltf",
  "/asset/Astronaut_BaseColor.png",
  "/favicon.ico",
];

// self.addEventListener("install", (installEvent) => {
//   installEvent.waitUntil(
//     caches.open(staticArApp).then((cache) => {
//       cache.addAll(assets);
//     })
//   );
// });

// self.addEventListener("fetch", (fetchEvent) => {
//   fetchEvent.respondWith(
//     caches.match(fetchEvent.request).then((res) => {
//       return res || fetch(fetchEvent.request);
//     })
//   );
// });
