# AR application

I use in this project the [ar.js](https://ar-js-org.github.io/AR.js-Docs/) library.

## Utilisation

To use this application you must first scan this qrcode.
<br>
<img src="/asset/qr-code.png" width="100px">

Then you have to point your phone to this hiro image.
<br>
<img src="/asset/hiro.png" width="100px">



You should see an astronaut appear.
